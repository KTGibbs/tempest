{
    "id": "44b95a94-df1a-4fa2-a078-e878f5f9ab63",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_house",
    "eventList": [
        {
            "id": "bb147c2e-3949-4232-bdc0-50cf08182ad5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "44b95a94-df1a-4fa2-a078-e878f5f9ab63"
        },
        {
            "id": "052cf2dc-7219-4901-a8cc-4783cc596898",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "44b95a94-df1a-4fa2-a078-e878f5f9ab63"
        },
        {
            "id": "1b7587b8-785c-43c8-8ad0-e0119e887264",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "44b95a94-df1a-4fa2-a078-e878f5f9ab63"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ffff7330-c5ce-4cff-a04d-86fd1f06b9f8",
    "visible": true
}