/// @desc
//Credit to Seth Coster of Butterscotch Shenanigans for
//The movement code setup!

o_controller.seconds_passed = delta_time/1000000;
o_controller.move_speed_this_frame = move_speed*o_controller.seconds_passed;

key_swap = keyboard_check_pressed(vk_space); //DEBUG
key_fire = keyboard_check_pressed(vk_enter);

move_xinput = 0;
move_yinput = 0;
//sprite_index = spr_cloud96x;
image_index = 0;

 
for (var i = 0; i < array_length_1d(movement_inputs); i++){
    var this_key = movement_inputs[i];
    if keyboard_check(this_key) {
        var this_angle = i*90;
        move_xinput += lengthdir_x(1, this_angle);
        move_yinput += lengthdir_y(1, this_angle);
    }
}
 
var moving = (point_distance(0,0,move_xinput,move_yinput) > 0 );
if moving  {
			move_dir = point_direction(0,0,move_xinput,move_yinput);
    x += lengthdir_x(o_controller.move_speed_this_frame, move_dir);
    y += lengthdir_y(o_controller.move_speed_this_frame, move_dir);



#region Change Sprite Based on Movement Direction

	//moving RIGHT
	if(move_xinput > 0 && !move_yinput){
		image_index = 1;
		image_xscale = 1;
	}
	//moving LEFT
	if(move_xinput < 0 && move_yinput = 0){
		image_index = 1;
		image_xscale = -1;
	}
	
	//moving DOWN
	if(move_xinput = 0 && move_yinput > 0){
		image_index = 3;
		image_xscale = 1;
	}
	
	//moving UP
	if(move_xinput = 0 && move_yinput < 0){
		image_index = 0;
		image_xscale = 1;
	}
	
	//moving UP RIGHT
	if(move_xinput > 0 && move_yinput < 0){
		image_index = 1;
		image_xscale = 1;
	}
	
	//moving UP LEFT
	if(move_xinput < 0 && move_yinput < 0){
		image_index = 1;
		image_xscale = -1;
	}
	
	//moving DOWN RIGHT
	if(move_xinput > 0 && move_yinput > 0){
		image_index = 2;
		image_xscale = 1;
	}
	
	//moving DOWN LEFT
	if(move_xinput < 0 && move_yinput > 0){
		image_index = 2;
		image_xscale = -1;
	}
}
#endregion

//Make switch sprite to angry if you're firing
if(key_fire && bulletCount < 10){
	instance_create_depth(x,y-16,-100,o_bullet);
	sprite_index = spr_cloudANGRY96x;
}


//Chance sprite on Space-press DEBUG
/*
	if(key_swap && sprite_index = spr_cloud96x){
		sprite_index = spr_cloudANGRY96x;
		exit;
	}
	
	if(key_swap && sprite_index = spr_cloudANGRY96x){
		sprite_index = spr_cloudWOKE96x;
		exit;
	}
	
	if(key_swap && sprite_index = spr_cloudWOKE96x){
		sprite_index = spr_cloud96x;
		exit;
	}

*/
	