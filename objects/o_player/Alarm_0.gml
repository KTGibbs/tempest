/// @desc Not smiley + no bullets, smile

//If there are no bullets on the screen and your sprite is not the default, make it so!
if(!instance_exists(o_bullet) && sprite_index != spr_cloud96x){
	sprite_index = spr_cloud96x;
}

alarm[0] = 100;