{
    "id": "c3e9032e-f175-4ab6-b77e-bfae5bb52d41",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_roof",
    "eventList": [
        {
            "id": "fbdfd03b-8761-4f82-bbec-1e590eaf03fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c3e9032e-f175-4ab6-b77e-bfae5bb52d41"
        },
        {
            "id": "1c9c5eea-261c-4331-81d9-671788fd7c98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c3e9032e-f175-4ab6-b77e-bfae5bb52d41"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "afe68f65-782f-4836-a4d1-665f3bb11381",
    "visible": true
}