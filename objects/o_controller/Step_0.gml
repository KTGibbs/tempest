switch(keyboard_key){
	case vk_escape: game_end(); break;
	case vk_f1: game_restart(); break;
}

seconds_passed = delta_time/1000000;
move_speed_this_frame = o_player.move_speed*seconds_passed;

//Kind of expecting this not to work for long
if(instance_exists(o_bullet)){
	bullet_move_speed_this_frame = o_bullet.move_speed*seconds_passed;
}
