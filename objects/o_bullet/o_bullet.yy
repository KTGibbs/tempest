{
    "id": "55ec1373-476d-4940-8829-6020f0719636",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_bullet",
    "eventList": [
        {
            "id": "f5faa702-a8cd-4e5e-8355-d78100547e28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "55ec1373-476d-4940-8829-6020f0719636"
        },
        {
            "id": "c9e217d3-a86b-4780-80db-cc7249683fee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "55ec1373-476d-4940-8829-6020f0719636"
        },
        {
            "id": "888153a1-e6f3-4451-8934-7c2f46646154",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "55ec1373-476d-4940-8829-6020f0719636"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "130e22fa-ded1-4b29-83c0-3abd9f91dd3f",
    "visible": true
}