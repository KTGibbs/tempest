{
    "id": "4739de56-b183-410f-91bf-de2dc958111f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_tree",
    "eventList": [
        {
            "id": "de578783-e599-47ae-ae35-7bb756053d4f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4739de56-b183-410f-91bf-de2dc958111f"
        },
        {
            "id": "d706992f-d22c-4654-a610-3f600ae97b52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4739de56-b183-410f-91bf-de2dc958111f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00c0d8e6-1de5-4e64-b7fd-637ee101ed42",
    "visible": true
}