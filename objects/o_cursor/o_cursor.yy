{
    "id": "e352d216-c3f9-4f72-afee-c9fa3223c865",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_cursor",
    "eventList": [
        {
            "id": "387c9d04-b101-4b0c-8c15-d3e54a9c3de2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e352d216-c3f9-4f72-afee-c9fa3223c865"
        },
        {
            "id": "23785d6a-2f6b-402f-91a3-7e70a462e8bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e352d216-c3f9-4f72-afee-c9fa3223c865"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0b396b30-06d2-4c37-8de7-399ef21ca747",
    "visible": true
}