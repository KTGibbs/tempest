{
    "id": "943a8571-9295-43a8-961a-572e10cc98a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CloudWOKE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 4,
    "bbox_right": 59,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a156b48-6dad-4600-b94a-0f9259f77aa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "943a8571-9295-43a8-961a-572e10cc98a9",
            "compositeImage": {
                "id": "8ced412a-e2e6-45dc-b03b-315fbf869e85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a156b48-6dad-4600-b94a-0f9259f77aa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9797fdd9-cd06-4784-8777-53edd5018a83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a156b48-6dad-4600-b94a-0f9259f77aa2",
                    "LayerId": "aa82296f-c6ed-4c2d-86c6-0eb875d9f78b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "aa82296f-c6ed-4c2d-86c6-0eb875d9f78b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "943a8571-9295-43a8-961a-572e10cc98a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}