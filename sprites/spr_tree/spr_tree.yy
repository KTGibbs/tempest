{
    "id": "00c0d8e6-1de5-4e64-b7fd-637ee101ed42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 573,
    "bbox_left": 0,
    "bbox_right": 242,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a1c0a86-3bbc-4c21-83d6-25bfcb945951",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00c0d8e6-1de5-4e64-b7fd-637ee101ed42",
            "compositeImage": {
                "id": "481466a0-c45f-4053-86ff-84fd70d0fdd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a1c0a86-3bbc-4c21-83d6-25bfcb945951",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e564f4dd-9135-4ced-931d-f4261dae5d6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a1c0a86-3bbc-4c21-83d6-25bfcb945951",
                    "LayerId": "c81ddb0a-a145-4d77-b14c-6e8ecc6f4df8"
                }
            ]
        },
        {
            "id": "5b4665c2-7d00-4154-9ec4-bd1e6ff8a30b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00c0d8e6-1de5-4e64-b7fd-637ee101ed42",
            "compositeImage": {
                "id": "54971068-4819-49ec-a00c-80487d4e5387",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b4665c2-7d00-4154-9ec4-bd1e6ff8a30b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcb5721b-907e-459a-996d-e3814468090c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b4665c2-7d00-4154-9ec4-bd1e6ff8a30b",
                    "LayerId": "c81ddb0a-a145-4d77-b14c-6e8ecc6f4df8"
                }
            ]
        },
        {
            "id": "2e180e51-e288-49fd-8416-b299d48d2ac1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00c0d8e6-1de5-4e64-b7fd-637ee101ed42",
            "compositeImage": {
                "id": "8feec645-2654-44b7-8299-c719160bc301",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e180e51-e288-49fd-8416-b299d48d2ac1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85b6e12c-9aad-4473-b76c-c384801799fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e180e51-e288-49fd-8416-b299d48d2ac1",
                    "LayerId": "c81ddb0a-a145-4d77-b14c-6e8ecc6f4df8"
                }
            ]
        },
        {
            "id": "652d3ae7-a27a-4f23-818d-9d0b1394f43e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00c0d8e6-1de5-4e64-b7fd-637ee101ed42",
            "compositeImage": {
                "id": "a5c3c897-a9eb-4fd9-ab46-1d4d730cefea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "652d3ae7-a27a-4f23-818d-9d0b1394f43e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07452b22-5f68-4d4e-a90d-6ac7cfcf2cfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "652d3ae7-a27a-4f23-818d-9d0b1394f43e",
                    "LayerId": "c81ddb0a-a145-4d77-b14c-6e8ecc6f4df8"
                }
            ]
        },
        {
            "id": "0d0168a2-16fc-41c3-9210-d8ba146e5a85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00c0d8e6-1de5-4e64-b7fd-637ee101ed42",
            "compositeImage": {
                "id": "e0e43fac-0d81-40c2-91c3-210d4aa7aee7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d0168a2-16fc-41c3-9210-d8ba146e5a85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "175961c9-fff8-492b-9b65-e90feebf124c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d0168a2-16fc-41c3-9210-d8ba146e5a85",
                    "LayerId": "c81ddb0a-a145-4d77-b14c-6e8ecc6f4df8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 574,
    "layers": [
        {
            "id": "c81ddb0a-a145-4d77-b14c-6e8ecc6f4df8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00c0d8e6-1de5-4e64-b7fd-637ee101ed42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 243,
    "xorig": 121,
    "yorig": 287
}