{
    "id": "afe68f65-782f-4836-a4d1-665f3bb11381",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_roof",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "989bdd42-056b-46cf-8513-408a3b4746e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afe68f65-782f-4836-a4d1-665f3bb11381",
            "compositeImage": {
                "id": "d0d45960-93c5-438a-8393-3265da88a8a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "989bdd42-056b-46cf-8513-408a3b4746e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6f1e672-4831-4331-ad61-7e7b76280845",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "989bdd42-056b-46cf-8513-408a3b4746e1",
                    "LayerId": "35c01cab-1651-44b1-b408-2f42e5d1bbb3"
                }
            ]
        },
        {
            "id": "6d8ab3ae-7202-42fb-9505-bc3f6e1f7fe9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afe68f65-782f-4836-a4d1-665f3bb11381",
            "compositeImage": {
                "id": "cb21fb1b-82d7-4ab2-9303-62bc0520e21a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d8ab3ae-7202-42fb-9505-bc3f6e1f7fe9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d423c986-2886-46ef-b273-0f6f18e127c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d8ab3ae-7202-42fb-9505-bc3f6e1f7fe9",
                    "LayerId": "35c01cab-1651-44b1-b408-2f42e5d1bbb3"
                }
            ]
        },
        {
            "id": "231b54a4-33c3-437a-bf23-a3eecc95d7a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afe68f65-782f-4836-a4d1-665f3bb11381",
            "compositeImage": {
                "id": "5ec56d4a-3b14-45c4-a175-d4bb18723d2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "231b54a4-33c3-437a-bf23-a3eecc95d7a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a599013-bd66-46be-a1dd-9c02aede52e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "231b54a4-33c3-437a-bf23-a3eecc95d7a6",
                    "LayerId": "35c01cab-1651-44b1-b408-2f42e5d1bbb3"
                }
            ]
        },
        {
            "id": "0f03102a-1354-401f-9bcc-c0ba96796b88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afe68f65-782f-4836-a4d1-665f3bb11381",
            "compositeImage": {
                "id": "7b248a2e-e89d-44f2-a9c9-1930fb7ea453",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f03102a-1354-401f-9bcc-c0ba96796b88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b066740-2950-4eb6-a14d-01fae4fb97b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f03102a-1354-401f-9bcc-c0ba96796b88",
                    "LayerId": "35c01cab-1651-44b1-b408-2f42e5d1bbb3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "35c01cab-1651-44b1-b408-2f42e5d1bbb3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afe68f65-782f-4836-a4d1-665f3bb11381",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 19
}