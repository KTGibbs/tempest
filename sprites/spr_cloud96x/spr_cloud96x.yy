{
    "id": "53824cb4-bdc1-4999-80f7-9ea1cb8f1e38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cloud96x",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e16ed1b-6fc4-4166-928f-8aecbd54d236",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53824cb4-bdc1-4999-80f7-9ea1cb8f1e38",
            "compositeImage": {
                "id": "738efec6-5400-494c-a3ef-5354f9deacfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e16ed1b-6fc4-4166-928f-8aecbd54d236",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03b899a2-734f-4b67-ba9f-5d9d0732a86f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e16ed1b-6fc4-4166-928f-8aecbd54d236",
                    "LayerId": "d8626499-812f-4051-9b16-30c6eb73bf96"
                }
            ]
        },
        {
            "id": "cf220267-ce08-4803-99c3-a25c5b4b75b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53824cb4-bdc1-4999-80f7-9ea1cb8f1e38",
            "compositeImage": {
                "id": "19208753-4870-4e5b-a5ce-5c1cb3822a10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf220267-ce08-4803-99c3-a25c5b4b75b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e3f1c94-ae00-4e70-aa05-af456bf172e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf220267-ce08-4803-99c3-a25c5b4b75b2",
                    "LayerId": "d8626499-812f-4051-9b16-30c6eb73bf96"
                }
            ]
        },
        {
            "id": "0d6755ee-a275-477f-a194-bdc2d68ddd6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53824cb4-bdc1-4999-80f7-9ea1cb8f1e38",
            "compositeImage": {
                "id": "adcfd757-21a8-465c-8670-8cb2bd9361dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d6755ee-a275-477f-a194-bdc2d68ddd6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42c54c02-6b3c-40b9-b58f-00b9adcc294d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d6755ee-a275-477f-a194-bdc2d68ddd6c",
                    "LayerId": "d8626499-812f-4051-9b16-30c6eb73bf96"
                }
            ]
        },
        {
            "id": "0945b6cc-17ca-4083-8717-088d5410ed28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53824cb4-bdc1-4999-80f7-9ea1cb8f1e38",
            "compositeImage": {
                "id": "92adfdd9-e156-4df2-acdf-242376c9e67b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0945b6cc-17ca-4083-8717-088d5410ed28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aef37314-dcaf-4de6-951e-e44378ab8f53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0945b6cc-17ca-4083-8717-088d5410ed28",
                    "LayerId": "d8626499-812f-4051-9b16-30c6eb73bf96"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "d8626499-812f-4051-9b16-30c6eb73bf96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53824cb4-bdc1-4999-80f7-9ea1cb8f1e38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}