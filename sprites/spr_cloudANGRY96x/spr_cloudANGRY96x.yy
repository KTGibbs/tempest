{
    "id": "40a2a57f-dd8b-4739-ba89-319fd092c58a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cloudANGRY96x",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f866bd05-73bf-4ad5-b48f-8e1965459b57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40a2a57f-dd8b-4739-ba89-319fd092c58a",
            "compositeImage": {
                "id": "4b3c0265-be85-4b6f-b8ae-80d64a9f35c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f866bd05-73bf-4ad5-b48f-8e1965459b57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad114e4d-cc8d-406c-877b-3f0d9840df26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f866bd05-73bf-4ad5-b48f-8e1965459b57",
                    "LayerId": "91b0a29f-e62b-4b96-ae04-8db913c9699c"
                }
            ]
        },
        {
            "id": "e26b05ba-823b-42e2-b175-1bf03e641a1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40a2a57f-dd8b-4739-ba89-319fd092c58a",
            "compositeImage": {
                "id": "cd166530-91bc-4a17-8049-28b2b39ed2c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e26b05ba-823b-42e2-b175-1bf03e641a1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed0c87d0-5834-4fd4-b10b-6038bf3f1a36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e26b05ba-823b-42e2-b175-1bf03e641a1a",
                    "LayerId": "91b0a29f-e62b-4b96-ae04-8db913c9699c"
                }
            ]
        },
        {
            "id": "08bdb578-a1a5-42e4-8adb-98854b061d93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40a2a57f-dd8b-4739-ba89-319fd092c58a",
            "compositeImage": {
                "id": "94b32e2d-af4d-4f0d-ae58-881cad351be2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08bdb578-a1a5-42e4-8adb-98854b061d93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2d8216c-8fe4-4320-bf78-92a06d4f2d35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08bdb578-a1a5-42e4-8adb-98854b061d93",
                    "LayerId": "91b0a29f-e62b-4b96-ae04-8db913c9699c"
                }
            ]
        },
        {
            "id": "f06b9c0c-3c8b-494e-a09f-e67b270f4a6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40a2a57f-dd8b-4739-ba89-319fd092c58a",
            "compositeImage": {
                "id": "8413526a-c8bb-4bbc-80fd-f348042e7540",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f06b9c0c-3c8b-494e-a09f-e67b270f4a6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b23ddcf-90b1-4441-b23a-559c08b37908",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f06b9c0c-3c8b-494e-a09f-e67b270f4a6a",
                    "LayerId": "91b0a29f-e62b-4b96-ae04-8db913c9699c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "91b0a29f-e62b-4b96-ae04-8db913c9699c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40a2a57f-dd8b-4739-ba89-319fd092c58a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}