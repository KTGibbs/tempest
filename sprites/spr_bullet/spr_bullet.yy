{
    "id": "130e22fa-ded1-4b29-83c0-3abd9f91dd3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc671219-5bec-4339-aa1f-ab7b34a8aa0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "130e22fa-ded1-4b29-83c0-3abd9f91dd3f",
            "compositeImage": {
                "id": "cde4a929-04ca-480c-b8f8-b8e8dae4c475",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc671219-5bec-4339-aa1f-ab7b34a8aa0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1780ca9-e172-4228-a626-6170d355db04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc671219-5bec-4339-aa1f-ab7b34a8aa0c",
                    "LayerId": "ccf28500-117f-40fe-8013-bfb4cb650919"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ccf28500-117f-40fe-8013-bfb4cb650919",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "130e22fa-ded1-4b29-83c0-3abd9f91dd3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}