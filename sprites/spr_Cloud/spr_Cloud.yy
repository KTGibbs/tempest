{
    "id": "cf319e07-a7c6-415f-919b-86479178e157",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Cloud",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 4,
    "bbox_right": 59,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f7f1151-22ff-4c4c-9c19-49582f869441",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf319e07-a7c6-415f-919b-86479178e157",
            "compositeImage": {
                "id": "409d6cbd-fb87-428e-b27a-c383a6e34acd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f7f1151-22ff-4c4c-9c19-49582f869441",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0cd4c0f-10e5-4634-8f57-d8decb793ec3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f7f1151-22ff-4c4c-9c19-49582f869441",
                    "LayerId": "1c6632b5-5f61-457d-89c1-0315c3cf6089"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1c6632b5-5f61-457d-89c1-0315c3cf6089",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf319e07-a7c6-415f-919b-86479178e157",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}