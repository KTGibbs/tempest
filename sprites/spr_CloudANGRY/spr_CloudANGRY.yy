{
    "id": "77969733-7139-4c4f-9e6d-a8f2c2be21a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CloudANGRY",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 4,
    "bbox_right": 59,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd14645c-c75d-4031-8a41-eb01c1d397b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77969733-7139-4c4f-9e6d-a8f2c2be21a6",
            "compositeImage": {
                "id": "0cb8451e-3685-4094-879f-2f767d31e9de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd14645c-c75d-4031-8a41-eb01c1d397b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a335b1ca-0f23-4b77-b514-3e8bb760f2a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd14645c-c75d-4031-8a41-eb01c1d397b2",
                    "LayerId": "4c3d4acc-b81e-4894-9c1c-07cc5aa4b9b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4c3d4acc-b81e-4894-9c1c-07cc5aa4b9b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77969733-7139-4c4f-9e6d-a8f2c2be21a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}