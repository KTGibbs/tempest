{
    "id": "0b396b30-06d2-4c37-8de7-399ef21ca747",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b0bd50a-2c98-4ed2-a090-1d969eeac7e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b396b30-06d2-4c37-8de7-399ef21ca747",
            "compositeImage": {
                "id": "1d43a6e6-9dce-478a-b967-224426b3fd11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b0bd50a-2c98-4ed2-a090-1d969eeac7e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d4b73b8-3f68-42f4-a58c-99cd99a2655c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b0bd50a-2c98-4ed2-a090-1d969eeac7e7",
                    "LayerId": "ec23877b-06c2-4d58-9c52-84f1143b6bbe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "ec23877b-06c2-4d58-9c52-84f1143b6bbe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b396b30-06d2-4c37-8de7-399ef21ca747",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}