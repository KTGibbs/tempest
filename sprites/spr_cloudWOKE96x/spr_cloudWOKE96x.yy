{
    "id": "5ffd7cc5-4b32-4e52-a3c6-d199878dec22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cloudWOKE96x",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98575e99-917d-4e14-8ae3-e3c2998c35cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ffd7cc5-4b32-4e52-a3c6-d199878dec22",
            "compositeImage": {
                "id": "56784fb3-ba69-450e-b1b4-3cdb5815edc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98575e99-917d-4e14-8ae3-e3c2998c35cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "006d1bcd-e151-41fb-97a7-73f22d002d03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98575e99-917d-4e14-8ae3-e3c2998c35cd",
                    "LayerId": "352c0b72-07c8-48a9-98b5-487a05ae47a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "352c0b72-07c8-48a9-98b5-487a05ae47a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ffd7cc5-4b32-4e52-a3c6-d199878dec22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}