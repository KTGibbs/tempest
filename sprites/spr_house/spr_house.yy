{
    "id": "ffff7330-c5ce-4cff-a04d-86fd1f06b9f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_house",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29c24e04-97ae-4dfc-8ad6-c6fcfda53ffb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffff7330-c5ce-4cff-a04d-86fd1f06b9f8",
            "compositeImage": {
                "id": "a27152d8-03d2-445b-a125-9c06dd51ab5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29c24e04-97ae-4dfc-8ad6-c6fcfda53ffb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "392b8eed-f6ed-478e-bc70-dad6fbfb0c84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c24e04-97ae-4dfc-8ad6-c6fcfda53ffb",
                    "LayerId": "805d5990-0457-45a2-b05f-10f97164f8da"
                }
            ]
        },
        {
            "id": "6147f34e-8786-4063-8e94-6fc44d16b74b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffff7330-c5ce-4cff-a04d-86fd1f06b9f8",
            "compositeImage": {
                "id": "7392b302-578a-42c6-a66f-71231d334368",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6147f34e-8786-4063-8e94-6fc44d16b74b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e893516-ddbe-468c-8a0a-5a3126b2ce25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6147f34e-8786-4063-8e94-6fc44d16b74b",
                    "LayerId": "805d5990-0457-45a2-b05f-10f97164f8da"
                }
            ]
        },
        {
            "id": "1f33973c-882c-4b79-acf6-8cf48ce0d9d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffff7330-c5ce-4cff-a04d-86fd1f06b9f8",
            "compositeImage": {
                "id": "8ca0800d-070a-4dbb-8371-35a38ef7c29d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f33973c-882c-4b79-acf6-8cf48ce0d9d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56fa0c5e-ad9b-4b6d-bfd0-b46e009f1f6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f33973c-882c-4b79-acf6-8cf48ce0d9d3",
                    "LayerId": "805d5990-0457-45a2-b05f-10f97164f8da"
                }
            ]
        },
        {
            "id": "0ada7f01-b372-418b-b22c-66cb20dc4436",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffff7330-c5ce-4cff-a04d-86fd1f06b9f8",
            "compositeImage": {
                "id": "cdbaa932-eac9-4a75-a0d8-d9d11aea9aa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ada7f01-b372-418b-b22c-66cb20dc4436",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "608d58c5-6c1e-48fd-aab3-ff252373138a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ada7f01-b372-418b-b22c-66cb20dc4436",
                    "LayerId": "805d5990-0457-45a2-b05f-10f97164f8da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "805d5990-0457-45a2-b05f-10f97164f8da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ffff7330-c5ce-4cff-a04d-86fd1f06b9f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4291559492,
        4294901972,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278219519,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4283148796,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}